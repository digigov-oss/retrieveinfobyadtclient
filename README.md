# retrieveInfoByADTClient

Client to connect on GSIS service.

Returns Natural Person data based on the ID card number

#### Example

```
import getInfo from '@digigov-oss/retreive-info-by-adt-client';
import config from './config.json'; 

const test = async () => {
  
   const input = {
    adt: "Α738249",
    kind: 1
   }
     try {
        const Info = await getInfo(input, config.user, config.pass);
        console.log(Info); 
    } catch (error) {
        console.log(error);
    }
}

test();
```

* you can use `overrides` to override the default values for auditrecord and input fields.
* for your tests, you don't need to use the `overrides` mechanism, in that case, the default storage path will be used ie `/tmp`
* look at [KED](https://www.gsis.gr/dimosia-dioikisi/ked/) standard guides for records you can use on auditInit"
Also, you can use `overrides` to override the default storage engine. Look at module [AuditRecordDB](https://gitlab.grnet.gr/digigov-oss/auditRecordDB/-/blob/main/README.md) for more details on how to use the AuditEngine.

#### Returns
an object like the following:
```
{
        retrieveInfosByADTRecord: {
                afm: '010800987',
                deactFlag: '1',
                doy: '1209',
                doyDescr: 'Δ΄ ΠΕΙΡΑΙΑ(ΣΤ΄,Δ΄ ΠΕΙΡΑΙΑ)',
                surname: 'ΣΤΑΘΟΠΟΥΛΟΣ',
                firstName: 'ΕΛΕΥΘ',
                fathersFirstName: 'ΣΠΥ',
                cntResidenceDescr: 'ΕΛΛΑΔΑ',
                residenceAddress: 'ΠΕΙΡΑΙΩΣ-ΑΘΗΝΩΝ',
                residenceNo: '69',
                parResidence: 18541,
                parResidenceDescr: 'ΠΕΙΡΑΙΑΣ'
        },
        callSequenceId: 49119007,
        callSequenceDate: Mon May 09 2022 10:44:07 GMT+0300 (Θερινή ώρα Ανατολικής Ευρώπης),
        errorRecord: null,
        auditUnit: 'gov.gr',
        auditTransactionId: '80',
        auditProtocol: '80/2022-05-09',
        auditTransactionDate: '2022-05-09T07:44:06Z',
        auditUserIp: '127.0.0.1',
        auditUserId: 'system'
}
```
or an error object like the following:
```
{
        retrieveInfosByADTRecord: null,
        callSequenceId: 49119009,
        callSequenceDate: Mon May 09 2022 10:45:13 GMT+0300 (Θερινή ώρα Ανατολικής Ευρώπης),
        errorRecord: {
                errorCode: 'RG_TAXPAYER_NF',
                errorDescr: 'Ανύπαρκτος Α.Φ.Μ.'
        },
        auditUnit: 'gov.gr',
        auditTransactionId: '82',
        auditProtocol: '82/2022-05-09',
        auditTransactionDate: '2022-05-09T07:45:12Z',
        auditUserIp: '127.0.0.1',
        auditUserId: 'system'
}
```
### Notes
In case of KED advertises wrong endpoint on production you have to use (override) the endpoint: `https://ked.gsis.gr/esb/registryService`
You can do that by setting the `endpoint` property on the `overrides` object.
```
const overrides = {
    endpoint: 'https://ked.gsis.gr/esb/registryService',
}
```