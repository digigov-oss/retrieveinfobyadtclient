import { AuditRecord, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
export declare type retrieveInfosByADTReturn = {
    cardNo: string;
    cardKindDescr: string;
    cardAuthDescr: string;
    cardDate: string;
    afm: string;
    deactFlag: string;
    assTxpActual: string;
    firmFlag: string;
    cntCitizenshipDescr: string;
    birthDate: string;
    birthPlace: string;
    deathDate: string;
    doy: string;
    doyDescr: string;
    surname: string;
    secondSurname: string;
    firstName: string;
    fathersFirstName: string;
    fathersSurname: string;
    mothersFirstName: string;
    mothersSurname: string;
    cntResidenceDescr: string;
    residenceAddress: string;
    residenceNo: string;
    parResidence: string;
    parResidenceDescr: string;
    postalAddress: string;
    postalNo: string;
    parPostal: string;
    parPostalDescr: string;
    code: string;
    description: string;
};
export declare type retrieveInfosByADTResponse = {
    retrieveInfosByADTRecord: retrieveInfosByADTReturn | null;
    callSequenceId: string;
    callSequenceDate: string;
    errorRecord: errorRecord | null;
};
export declare type errorRecord = {
    errorCode: string;
    errorDescr: string;
};
export declare type kind = [
    "ΑΓΝΩΣΤΗ",
    "ΑΤ ΑΣΤΥΝΟΜΙΚΗ ΤΑΥΤΟΤΗΤΑ",
    "ΕΣ ΕΛΛΗΝΙΚΟΣ ΣΤΡΑΤΟΣ",
    "ΠΝ ΠΟΛΕΜΙΚΟ ΝΑΥΤΙΚΟ",
    "ΠΑ ΠΟΛΕΜΙΚΗ ΑΕΡΟΠΟΡΙΑ",
    "ΕΑ ΕΛΛΗΝΙΚΗ ΑΣΤΥΝΟΜΙΑ",
    "ΛΣ ΛΙΜΕΝΙΚΟ ΣΩΜΑ",
    "ΠΣ ΠΥΡΟΣΒΕΣΤΙΚΟ ΣΩΜΑ",
    "ΔΙ ΔΙΑΒΑΤΗΡΙΟ",
    "ΚΑ ΚΕΝΤΡΟ ΑΛΛΟΔΑΠΩΝ",
    "ΧΤ ΧΩΡΙΣ ΤΑΥΤΟΤΗΤΑ",
    "ΕΝΤΟΛΕΑΣ ΦΠ",
    "ΚΠ ΚΑΡΤΑ ΠΑΡΑΜΟΝΗΣ ΠΕΡΙΟΡ ΧΡΟΝ ΔΙΑΡΚ",
    "ΤΑΥΤΟΤΗΤΑ ΕΥΡΩΠΑΙΚΗΣ ΕΝΩΣΗΣ",
    "ΤΑΥΤΟΤΗΤΑ ΑΝΙΘΑΓΕΝΗ(Ν.139/1975)",
    "ΤΑΥΤΟΤΗΤΑ ΟΜΟΓΕΝΟΥΣ",
    "ΑΡΙΘΜΟΣ ΚΟΙΝΩΝΙΚΗΣ ΑΣΦΑΛΙΣΗΣ ΗΠΑ",
    "ΕΔΤ ΠΟΛΙΤΙΚΟΥ ΠΡΟΣΦΥΓΑ ΥΠΟ ΑΝΑΓΝΩΡΙΣΗ",
    "ΕΙΔΙΚΟ ΔΕΛΤΙΟ ΠΟΛΙΤΙΚΟΥ ΦΥΓΑΔΑ",
    "ΑΔΕΙΑ ΠΑΡΑΜΟΝΗΣ ΠΡΟΣΦΥΓΑ ΑΤΕΛΩΣ",
    "ΕΙΔΙΚΟ ΔΕΛΤΙΟ ΠΡΟΣΦΥΓΑ ΑΝΘΡΩΠ ΚΑΘΕΣΤΩΤΟΣ",
    "ΤΑΥΤΟΤΗΤΑ ΑΛΛΟΔΑΠΟΥ ΚΡΑΤΟΥΜΕΝΟΥ",
    "ΠΙΣΤΟΠΟΙΗΤΙΚΟ ΓΕΝΝΗΣΗΣ ΑΛΛΟΔΑΠΟΥ",
    "ΤΑΥΤΟΤΗΤΑ ΧΩΡΑΣ ΕΚΤΟΣ ΕΥΡΩΠΑΙΚΗΣ ΕΝΩΣΗΣ",
    "ΑΓ ΕΛΛΗΝΙΚΗ ΑΓΡΟΦΥΛΑΚΗ",
    "ΔΕΛΤΙΟ ΑΙΤΟΥΝΤΟΣ ΔΙΕΘΝΗ ΠΡΟΣΤΑΣΙΑ",
    "ΑΔΕΙΑ ΔΙΑΜ ΥΠΗΚ ΤΡΙΤ ΧΩΡ ΧΩΡΙΣ ΔΙΑΒΑΤ",
    "ΑΔΕΤ/ΔΙΚΑΙΟΥΧΟΣ ΔΙΕΘΝΟΥΣ ΠΡΟΣΤΑΣΙΑΣ",
    "ΤΑΞΙΔΙΩΤΙΚΟ ΕΓΓΡΑΦΟ"
];
export declare type retrieveInfosByADTInputRecord = {
    adt: string;
    kind: number;
};
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {string} endpoint - The endpoint to connect to
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export declare type overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 *
 * @param input retrieveInfosByADTInputRecord;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns searchIdentitiesByNameOutputRecord | errorRecord
 */
export declare const getInfo: (input: retrieveInfosByADTInputRecord, user: string, pass: string, overrides?: overrides | undefined) => Promise<any>;
export default getInfo;
