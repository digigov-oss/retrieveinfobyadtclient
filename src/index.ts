import soapClient from './soapClient.js';
import {generateAuditRecord, AuditRecord, FileEngine, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
import config from './config.json'; 

export declare type retrieveInfosByADTReturn = {
    cardNo: string;
    cardKindDescr: string;
    cardAuthDescr: string;
    cardDate: string;
    afm: string;
    deactFlag: string;
    assTxpActual: string;
    firmFlag: string;
    cntCitizenshipDescr: string;
    birthDate: string;
    birthPlace: string;
    deathDate: string;
    doy: string;
    doyDescr: string;
    surname: string;
    secondSurname: string;
    firstName: string;
    fathersFirstName: string;
    fathersSurname: string;
    mothersFirstName: string;
    mothersSurname: string;
    cntResidenceDescr: string;
    residenceAddress: string;
    residenceNo: string;
    parResidence: string;
    parResidenceDescr: string;
    postalAddress: string;
    postalNo: string;
    parPostal: string;
    parPostalDescr: string;
    code: string;
    description: string;
}

export declare type retrieveInfosByADTResponse = {
    retrieveInfosByADTRecord: retrieveInfosByADTReturn | null;
    callSequenceId: string;
    callSequenceDate: string;
    errorRecord:errorRecord | null;
}

export declare type errorRecord = {
    errorCode:string;
    errorDescr:string;
}
export declare type kind = [ //in order
    "ΑΓΝΩΣΤΗ", //0
    "ΑΤ ΑΣΤΥΝΟΜΙΚΗ ΤΑΥΤΟΤΗΤΑ", //1
    "ΕΣ ΕΛΛΗΝΙΚΟΣ ΣΤΡΑΤΟΣ", //2 
    "ΠΝ ΠΟΛΕΜΙΚΟ ΝΑΥΤΙΚΟ", //3 
    "ΠΑ ΠΟΛΕΜΙΚΗ ΑΕΡΟΠΟΡΙΑ", //4 
    "ΕΑ ΕΛΛΗΝΙΚΗ ΑΣΤΥΝΟΜΙΑ", //5 
    "ΛΣ ΛΙΜΕΝΙΚΟ ΣΩΜΑ", //6 
    "ΠΣ ΠΥΡΟΣΒΕΣΤΙΚΟ ΣΩΜΑ", //7 
    "ΔΙ ΔΙΑΒΑΤΗΡΙΟ", //8 
    "ΚΑ ΚΕΝΤΡΟ ΑΛΛΟΔΑΠΩΝ", //9 
    "ΧΤ ΧΩΡΙΣ ΤΑΥΤΟΤΗΤΑ", //10 
    "ΕΝΤΟΛΕΑΣ ΦΠ", //11 
    "ΚΠ ΚΑΡΤΑ ΠΑΡΑΜΟΝΗΣ ΠΕΡΙΟΡ ΧΡΟΝ ΔΙΑΡΚ", //12 
    "ΤΑΥΤΟΤΗΤΑ ΕΥΡΩΠΑΙΚΗΣ ΕΝΩΣΗΣ", //13 
    "ΤΑΥΤΟΤΗΤΑ ΑΝΙΘΑΓΕΝΗ(Ν.139/1975)", //14 
    "ΤΑΥΤΟΤΗΤΑ ΟΜΟΓΕΝΟΥΣ", //15 
    "ΑΡΙΘΜΟΣ ΚΟΙΝΩΝΙΚΗΣ ΑΣΦΑΛΙΣΗΣ ΗΠΑ", //16 
    "ΕΔΤ ΠΟΛΙΤΙΚΟΥ ΠΡΟΣΦΥΓΑ ΥΠΟ ΑΝΑΓΝΩΡΙΣΗ", //17 
    "ΕΙΔΙΚΟ ΔΕΛΤΙΟ ΠΟΛΙΤΙΚΟΥ ΦΥΓΑΔΑ", //18
    "ΑΔΕΙΑ ΠΑΡΑΜΟΝΗΣ ΠΡΟΣΦΥΓΑ ΑΤΕΛΩΣ", //19
    "ΕΙΔΙΚΟ ΔΕΛΤΙΟ ΠΡΟΣΦΥΓΑ ΑΝΘΡΩΠ ΚΑΘΕΣΤΩΤΟΣ", //20
    "ΤΑΥΤΟΤΗΤΑ ΑΛΛΟΔΑΠΟΥ ΚΡΑΤΟΥΜΕΝΟΥ", //21
    "ΠΙΣΤΟΠΟΙΗΤΙΚΟ ΓΕΝΝΗΣΗΣ ΑΛΛΟΔΑΠΟΥ", //22
    "ΤΑΥΤΟΤΗΤΑ ΧΩΡΑΣ ΕΚΤΟΣ ΕΥΡΩΠΑΙΚΗΣ ΕΝΩΣΗΣ", //23
    "ΑΓ ΕΛΛΗΝΙΚΗ ΑΓΡΟΦΥΛΑΚΗ", //24
    "ΔΕΛΤΙΟ ΑΙΤΟΥΝΤΟΣ ΔΙΕΘΝΗ ΠΡΟΣΤΑΣΙΑ", //25
    "ΑΔΕΙΑ ΔΙΑΜ ΥΠΗΚ ΤΡΙΤ ΧΩΡ ΧΩΡΙΣ ΔΙΑΒΑΤ", //26
    "ΑΔΕΤ/ΔΙΚΑΙΟΥΧΟΣ ΔΙΕΘΝΟΥΣ ΠΡΟΣΤΑΣΙΑΣ", //27
    "ΤΑΞΙΔΙΩΤΙΚΟ ΕΓΓΡΑΦΟ" //28
]

export declare type retrieveInfosByADTInputRecord = {
    adt: string;
    kind: number; //lookup from kind array
}

/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {string} endpoint - The endpoint to connect to
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
 export declare type overrides = {
    endpoint?:string;
    prod?:boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
 }

/**
 * 
 * @param input retrieveInfosByADTInputRecord;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns searchIdentitiesByNameOutputRecord | errorRecord
 */
 export const getInfo = async (input:retrieveInfosByADTInputRecord, user:string, pass:string, overrides?:overrides | undefined ) => {
    const endpoint = overrides?.endpoint ?? "";
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {} as AuditRecord;
    const auditStoragePath = overrides?.auditStoragePath ?? "/tmp"
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod==true? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord) throw new Error('Audit record is not initialized');
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const Info = await s.getInfo(input);
        return {...Info,...auditRecord};
       } catch (error) {
           throw(error);
       }
}
export default getInfo;
