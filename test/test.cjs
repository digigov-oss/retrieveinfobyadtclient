const getInfo = require('../dist/cjs/index.js').default;
const config = require('./config.json');
const inspect = require('object-inspect');
const test = async () => {

    const input = {
     adt: "Α738249",
     kind: 1
    }
  
     try {
         const Info = await getInfo(input, config.user, config.pass);
         console.log(inspect(Info,{depth:10,indent:"\t"})); 
     } catch (error) {
         console.log(error);
     }
     
 
 
 }
 
 test();