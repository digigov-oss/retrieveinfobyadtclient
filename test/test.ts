import getInfo from '../src/index';
import config from './config.json'; 
import inspect from 'object-inspect';
const test = async () => {
   
   const input = {
    adt: "Α738249",
    kind: 1
   }
 
    try {
        const Info = await getInfo(input, config.user, config.pass);
        console.log(inspect(Info,{depth:10,indent:"\t"})); 
    } catch (error) {
        console.log(error);
    }
    


}

test();